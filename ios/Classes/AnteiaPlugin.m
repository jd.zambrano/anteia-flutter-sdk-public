#import "AnteiaPlugin.h"
#if __has_include(<anteia/anteia-Swift.h>)
#import <anteia/anteia-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "anteia-Swift.h"
#endif

@implementation AnteiaPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftAnteiaPlugin registerWithRegistrar:registrar];
}
@end
