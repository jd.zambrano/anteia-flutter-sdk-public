
import 'package:anteia/dtos/registration/Registration.dart';

class ApiProvider {
  static const String _API_ENDPOINT = "https://test-api.anteia.co";

  Future<Registration> getRegistration({String id}) async {
    var reg = Registration();
    //basic data
    reg.clientId = "testClientId";
    reg.country = "COL";
    reg.customData = Map.from({"key1": "value1", "key2": "value2"});
    reg.devices = List.from([
      UserDeviceInfo()
        ..deviceId = "testDevideID"
        ..ip = "192.168.0.1"
        ..numCpus = "1"
        ..os = "testOS"
        ..osVersion = "1.0"
        ..screenHeigh = 1920
        ..screenWidth = 1080
        ..trusted = true
        ..type = "Mobile"
    ]);
    reg.bioResponses = List.from([
      BiometricResponse()
        ..data = "{\"result\":\"true\"}"
        ..dataPath = ""
        ..registrationId = "testRegistrationId"
        ..requirementId = "testRequirementId"
        ..result = true
    ]);
    reg.blockingListsAlert = false;
    reg.documentValidity = DocumentValidityCheck()
      ..alterationCheck = true
      ..ocrCheck = true
      ..taxonomyCheck = true
      ..templateCheck = true;
    reg.documentValidityCheck = true;
    reg.email = "testEmail@testDomain.com";
    reg.endDatetime = DateTime.now();
    //External query
    var listResult = List.of([
      ListResult()
        ..documentMatch = false
        ..errorString = null
        ..isBlocking = true
        ..listName = "testListName"
        ..nameMatch = false
        ..result = false
        ..resultDetails = null
        ..risk = false
        ..warning = false
    ]);
    var exteralQueryResults = List.of([
      ExternalQueryResult()
        ..externalEntityId = "testEntityId"
        ..externalEntityName = "testEntity"
        ..idConsultado = "1234"
        ..listResults = listResult
        ..numConsultedLists = 1
        ..numResultsWithRisk = 0
        ..result = true
        ..risk = false
        ..tipoIdConsultado = "testId"
        ..warning = false
    ]);
    reg.externalQueryResults = exteralQueryResults;
    reg.faceMatchPercent = 75.0;
    reg.governmentData = GovernmentData()
      ..birthDate = DateTime.now()
      ..document = "1234"
      ..entityName = "testGovernmentEntity"
      ..expeditionDate = DateTime.now()
      ..expirationDate = DateTime.now()
      ..firstName = "Fist"
      ..fullName = "First Middle Last Secondlast"
      ..lastName = "Last"
      ..middleName = "Middle"
      ..secondLastName = "Secondlast"
      ..sex = "M";
    reg.governmentDataCheck = true;
    reg.hasAlerts = false;
    reg.idConfirmed = true;
    reg.idMatch = true;
    reg.initialData = InitialData()
      ..email = "testEmail@testDomain.com"
      ..identification = "1234"
      ..lastName = "Last"
      ..phoneNumber = "3990000000";
    reg.initialDataCheck = true;
    reg.isNaturalPerson = true;
    reg.lastName = "Last";
    reg.location = UserLocation()
      ..address = "testAddress"
      ..adminArea = "testCity"
      ..country = "testCountry"
      ..lat = 0.0
      ..locality = "testLocality"
      ..lon = 0.0
      ..postalCode = "12345"
      ..state = "testState"
      ..trusted = true
      ..userId = "testUserId";
    reg.name = "First";
    reg.needsManualRevision = false;
    reg.needsPhonePhoto = false;
    reg.nit = null;
    reg.phone = "3990000000";
    reg.projectId = "testProjectId";
    reg.questionResponses = List.empty();
    reg.redirectionUrl = null;
    reg.result = true;
    reg.startDatetime = DateTime.now();
    reg.txHash = null;
    reg.userId = "testUserId";
    reg.userIdType = "testDocIdType";
    reg.userIdentification = "1234";
    reg.videoPath = "testVideoPath";
    reg.voiceSaved = true;
    
    return reg;
  }

  String getToken({String apiKey}) {
    return "ASDF";
  }

  Future<bool> testAuth({String apiKey}) {
    return Future.delayed(Duration(milliseconds: 1), () => true);
  }
}
