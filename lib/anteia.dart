import 'dart:async';

import 'package:anteia/dtos/AnteiaPluginConfig.dart';
import 'package:anteia/dtos/AnteiaRegistrationPreloadedData.dart';
import 'package:flutter/services.dart';

import 'dtos/registration/Registration.dart';
import 'exceptions/Exceptions.dart';
import 'providers/ApiProvider.dart';

class AnteiaSDK {
  static bool isInitialized = false;
  static const MethodChannel _channel = const MethodChannel('anteia');
  static AnteiaPluginConfig pluginConfig;
  static ApiProvider _apiProvider = ApiProvider();
  static Future<String> get platformVersion async {
    final String version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }

  static Future<void> initSdk({AnteiaPluginConfig config}) async {
    pluginConfig = config;
    isInitialized = true;
    //Test the auth
    var res = await _apiProvider.testAuth(apiKey: pluginConfig.apiKey);
    if (!res) throw UnauthorizedException("Invalid API Key");
    return Future.delayed(Duration(milliseconds: 1));
  }

  /**
   * @returns the registrationId
   * Returns the registrationId that was the result of the onboarding flow
   */
  static Future<String> startOnboarding(
      {AnteiaRegistrationPreloadedData data, String projectId}) async {
    if (!isInitialized)
      throw new NotInitializedException("SDK is not initialized!");
    final String id = "testClientId";
    return id;
  }

  static Future<Registration> getRegistration({String id}) async {
    if (!isInitialized)
      throw new NotInitializedException("SDK is not initialized!");
    return Future.delayed(
        Duration(milliseconds: 1), () => _apiProvider.getRegistration(id: id));
  }

  static Future<bool> checkPassword(String sha256hash) async {
    if (!isInitialized)
      throw new NotInitializedException("SDK is not initialized!");
    return Future.delayed(Duration(milliseconds: 1), () => true);
  }
}
