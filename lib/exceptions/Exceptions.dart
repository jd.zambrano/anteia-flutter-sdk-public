class NotInitializedException implements Exception {
  String cause;
  NotInitializedException(this.cause);
}

class NotFoundException implements Exception {
  String cause;
  NotFoundException(this.cause);
}

class UnauthorizedException implements Exception {
  String cause;
  UnauthorizedException(this.cause);
}
