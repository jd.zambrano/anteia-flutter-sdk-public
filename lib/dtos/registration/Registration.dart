class Registration {
  String projectId;
  String clientId;
  String userId;
  String userIdType;
  String userIdentification;
  bool idConfirmed;
  bool idMatch;
  bool isNaturalPerson;
  String name;
  String lastName;
  String nit;
  DateTime startDatetime;
  String email;
  String phone;
  DateTime endDatetime;
  String videoPath;
  String txHash;
  bool result;
  List<QuestionResponse> questionResponses;
  List<BiometricResponse> bioResponses;
  List<UserDeviceInfo> devices;
  UserLocation location;
  List<ExternalQueryResult> externalQueryResults;
  String redirectionUrl;
  bool needsManualRevision;
  bool hasAlerts;
  double faceMatchPercent;
  bool voiceSaved;
  bool needsPhonePhoto;
  String country;
  GovernmentData governmentData;
  bool governmentDataCheck;
  DocumentValidityCheck documentValidity;
  bool documentValidityCheck;
  bool blockingListsAlert;
  Map customData;
  InitialData initialData;
  bool initialDataCheck;
}

class InitialData {
  String identification;
  String lastName;
  String phoneNumber;
  String email;
}

class DocumentValidityCheck {
  bool templateCheck;
  bool taxonomyCheck;
  bool ocrCheck;
  bool alterationCheck;
}

class GovernmentData {
  String entityName;
  String document;
  String fullName;
  String firstName;
  String middleName;
  String lastName;
  String secondLastName;
  DateTime expirationDate;
  DateTime expeditionDate;
  String sex;
  DateTime birthDate;
}

class ExternalQueryResult {
  bool result;
  String externalEntityName;
  String externalEntityId;
  String idConsultado;
  String tipoIdConsultado;
  bool risk;
  bool warning;
  int numConsultedLists;
  int numResultsWithRisk;
  List<ListResult> listResults;
}

class ListResult {
  String listName;
  List<String> resultDetails;
  bool result;
  bool risk;
  bool warning;
  String errorString;
  bool isBlocking;
  bool nameMatch;
  bool documentMatch;
}

class UserLocation {
  double lat;
  double lon;
  bool trusted;
  String userId;
  String locality;
  String adminArea;
  String state;
  String country;
  String postalCode;
  String address;
}

class UserDeviceInfo {
  String os;
  String osVersion;
  String deviceId;
  String userId;
  int screenHeigh;
  int screenWidth;
  bool trusted;
  String type;
  String ip;
  String numCpus;
}

class BiometricResponse {
  String registrationId;
  String requirementId;
  String data;
  String dataPath;
  bool result;
}

class QuestionResponse {
  String response;
  String videoTimestamp;
  String question;
  String questionId;
  String registrationId;
  String responseId;
}
