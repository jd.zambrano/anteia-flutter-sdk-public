class AnteiaPluginConfig {
  final String apiKey;


  final String clientId;

  final bool devMode;

  AnteiaPluginConfig(
      {this.devMode = true, this.apiKey, this.clientId});
}
