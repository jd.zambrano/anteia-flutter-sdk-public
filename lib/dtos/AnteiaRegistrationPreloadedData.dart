class AnteiaRegistrationPreloadedData {
  final String email;
  final String phoneNumber;
  final String lastName;
  final String identification;

  AnteiaRegistrationPreloadedData(
      {this.email, this.phoneNumber, this.lastName, this.identification});
}
