import 'package:anteia/dtos/registration/Registration.dart';
import 'package:flutter/material.dart';
import 'dart:async';

import 'package:flutter/services.dart';
import 'package:anteia/anteia.dart';
import 'package:anteia/dtos/AnteiaPluginConfig.dart';
import 'package:anteia/dtos/AnteiaRegistrationPreloadedData.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String _platformVersion = 'Unknown';
  String _id = '';

  @override
  void initState() {
    super.initState();
    initPlatformState();
  }

  Future<void> initPlatformState() async {
    String platformVersion;
    try {
      platformVersion = await AnteiaSDK.platformVersion;
    } on PlatformException {
      platformVersion = 'Failed to get platform version.';
    }
    if (!mounted) return;

    setState(() {
      _platformVersion = platformVersion;
    });
  }

  navigate() async {
    String id = await AnteiaSDK.startOnboarding(
        data: AnteiaRegistrationPreloadedData(), projectId: "");
    setState(() {
      _id = id;
    });
    print(id);
  }

  initSdk() async {
    AnteiaPluginConfig config = AnteiaPluginConfig(
      apiKey: "testAPiKey",
      clientId: "testClientId",
    );
    var reg = await AnteiaSDK.initSdk(config: config);
    setState(() {});
  }

  getRegistration() async {
    var reg = await AnteiaSDK.getRegistration(id: _id);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('RegistrationId: $_id\n'),
              Text("Is Initialized: ${AnteiaSDK.isInitialized}"),
              FlatButton(
                child: Text("Init"),
                onPressed: initSdk,
              ),
              FlatButton(
                child: Text("Navegar"),
                onPressed: () => navigate(),
              ),
              FlatButton(
                child: Text("Get Reg"),
                onPressed: getRegistration,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
